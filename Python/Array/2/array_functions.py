
#All array functions

import array as arr

name = arr.array('u', ['R','o','h','i','t'])
#print(name)

data = arr.array('i', [10,20,30,40,50])
print(data)

data.append(60)
print(data)

print(id(data))
print(data.buffer_info())   # buffer_info jya buffer madhe array stored ahe tyacha address deto aani length pn deto.

data.append(40)         # 10 20 30 40 50 60 40
print(data.count(40))   # 2

data.extend([100,200])
print(data)

lst = [101,102]
data.fromlist(lst)

print(data)

print(data.index(10))

data.insert(5,500)
print(data)

data.pop()
print(data)

data.remove(101)
print(data)

data.reverse()
print(data)

copyList = data.tolist()
print(copyList)

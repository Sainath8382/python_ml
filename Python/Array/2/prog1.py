
# 3 Ways of using arrays

'''
import array as arr

data = arr.array('i',[1,2,4,5])
print(data)
'''

import array 

data = array.array('i',[1,2,4,5])
print(data)

from array import *

data = array('i', [10,20,30,40])
print(data)


class Employee:

    def __init__(self,empname, empid):
        self.empname = empname
        self.empid = empid

    def empInfo(self):
        print(self.empname)
        print(self.empid)

emp1 = Employee("SAM", 10)
emp2 = Employee("AShish", 15)

emp1.empInfo()
emp2.empInfo()



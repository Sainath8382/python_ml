
class Company:

    compName = "Facebook"

    def __init__(self):
        self.empId = 12
        self.empName = "Ashish"

    def compInfo(self):
        print(self.empId)
        print(self.empName)
        print(self.compName)
        print(self)

        #print(Company.compName)
    @classmethod
    def CompData(cls):
        print(Company.compName)
        print(cls)

    @staticmethod
    def company():
        print(Company.compName)

obj1 = Company()
obj1.compName = "Meta"
#obj1.compInfo()
#obj1.CompData()

obj2 = Company()
#obj2.compInfo()
#obj2.CompData()
obj2.company()

Company.compName = "Meta"

#obj1.compInfo()
#obj2.compInfo()


#Function decorator pre required code 1

def OuterFunc():

    print("In outer func")

    def innerFun1():
        print("In inner function")

    def innerFun2():
        print("In inner function 2")

    innerFun1()
    innerFun2()

#OuterFunc()

retFun = OuterFunc

print(retFun())


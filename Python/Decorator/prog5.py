
#Function decorator code 2

def mult(fun):

    def sqr(x):
        ans = fun(x)
        return ans * x

    return sqr

@mult
def sqr(x):

    return x*x

print(sqr(5))


# Actual function decorator code:
# Decorator is a Design pattern. It adds some extra features into the function which are not visible to the user

def decorfun(func):

    def wrapper():
        print("Start wrapper")
        func()
        print("End wrapper")

    return wrapper

@decorfun
def Normalfun():

    print("Hello in normal function")

#normalfun = decorfun(Normalfun)
Normalfun()

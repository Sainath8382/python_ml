
# Decorator chaining : Use multiple decorators in a single code for a single function

def reverse(fun):

    def inner(*args):
        str1 = fun(*args)
        return str1[-1: :-1]

    return inner

def concat(fun):

    def inner(*args):
        str1 = fun(*args)
        str2 = ' '

        for x in str1:
            str2 = str2 + x
        return str2

    return inner

@reverse
@concat
def printStr(str1, str2):

    return str1, str2

print(printStr("Shashi", "Bagal"))
